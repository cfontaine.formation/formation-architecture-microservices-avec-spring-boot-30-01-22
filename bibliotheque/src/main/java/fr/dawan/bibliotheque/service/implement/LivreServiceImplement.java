package fr.dawan.bibliotheque.service.implement;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.repositories.LivreRepository;
import fr.dawan.bibliotheque.service.LivreService;

public class LivreServiceImplement implements LivreService {

    @Autowired 
    LivreRepository livreRepository;
    
    @Autowired
    ModelMapper modelMapper;
    
    @Override
    public List<LivreDto> getAllLivre() {
        return livreRepository.findAll().stream().map(l->modelMapper.map(l,LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public List<LivreDto> getAllLivre(Pageable page) {
        return livreRepository.findAll(page).stream().map(l->modelMapper.map(l,LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public LivreDto findById(long id) {
        return modelMapper.map(livreRepository.findById(id).get(),LivreDto.class);
    }

    @Override
    public List<LivreDto> searchByTitre(String titre) {
        return livreRepository.findByTitreLikeIgnoreCase("%"+titre+"%").stream().map(l->modelMapper.map(l,LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public void deleteById(long id) {
        livreRepository.deleteById(id);
    }

    @Override
    public LivreDto saveOrUpdate(LivreDto lvrDto) {
        Livre l= livreRepository.saveAndFlush(modelMapper.map(lvrDto, Livre.class));
        return modelMapper.map(l, LivreDto.class);
    }

}
