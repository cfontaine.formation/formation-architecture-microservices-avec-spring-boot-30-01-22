package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.bibliotheque.entities.Livre;

@Repository
public interface LivreRepository extends JpaRepository<Livre, Long> {

    List<Livre> findByTitreLikeIgnoreCase(String recherche);

    List<Livre> findByAnneeSortie(int anneeSortie);

    List<Livre> findByAnneeSortieBetween(int anneeMin, int anneMax);

}
