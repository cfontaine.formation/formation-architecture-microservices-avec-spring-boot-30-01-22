package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.bibliotheque.entities.Auteur;

@Repository
public interface AuteurRepository extends JpaRepository<Auteur, Long> {

    List<Auteur> findByDecesIsNull();

    Page<Auteur> findByDecesIsNull(Pageable pageable);

    List<Auteur> findByNom(String nom);

    @Query(value = "SELECT a FROM Auteur a Join a.livres l WHERE l.id=:idLivre")
    List<Auteur> findByLivreId(@Param("idLivre") long idLivre);

    // @Query(value= "SELECT a FROM Auteur a Join a.livres l GROUP BY a.id ORDER BY COUNT(l.id) DESC")
    @Query(value = "SELECT auteurs.id, auteurs.prenom, auteurs.nom,auteurs.deces, auteurs.NAISSANCE, auteurs.VERSION , auteurs.NATION_ID FROM auteurs "
            + "INNER JOIN  auteurs_livres ON auteurs.id=auteurs_livres.auteurs_id "
            + "GROUP BY auteurs.id ORDER BY COUNT(auteurs_livres.livres_id) DESC LIMIT 5", nativeQuery = true)
    List<Auteur> findBytop5Auteur();
}
