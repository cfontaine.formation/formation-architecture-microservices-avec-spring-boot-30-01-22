package fr.dawan.bibliotheque;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.bibliotheque.entities.Auteur;
import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.entities.Nation;
import fr.dawan.bibliotheque.repositories.AuteurRepository;
import fr.dawan.bibliotheque.repositories.LivreRepository;
import fr.dawan.bibliotheque.repositories.NationRepository;

@SpringBootTest
@ActiveProfiles("DEV")
public class RepositoryMain {

    @Autowired
    NationRepository nationRepository;
    
    @Autowired
    LivreRepository livreRepository; 
    
    @Autowired
    AuteurRepository auteurRepository;
    
    
    @Test
    public void nationRep() {
        List<Nation> lstNation=nationRepository.findByNom("Italie");
        for(Nation n : lstNation) {
            System.out.println(n);
        }
    }
    
    @Test
    public void livreRep() {
        List<Livre> lstLivre=livreRepository.findByTitreLikeIgnoreCase("d___%");
        for(Livre l : lstLivre) {
            System.out.println(l);
        }
        
        lstLivre=livreRepository.findByAnneeSortie(1992);
        for(Livre l : lstLivre) {
            System.out.println(l);
        }
        
        lstLivre=livreRepository.findByAnneeSortieBetween(1980,1990);
        for(Livre l : lstLivre) {
            System.out.println(l);
        }

    }
    
    @Test
    public void auteurRep() {
        List<Auteur> lstAuteur=auteurRepository.findByDecesIsNull();
        for(Auteur a : lstAuteur) {
            System.out.println(a);
        }
        

        
        lstAuteur=auteurRepository.findByDecesIsNull(PageRequest.of(1,2)).getContent();
        for(Auteur a : lstAuteur) {
            System.out.println(a);
        }
        
        lstAuteur=auteurRepository.findByNom("Gibson");
        for(Auteur a : lstAuteur) {
            System.out.println(a);
        }
        
        lstAuteur=auteurRepository.findByLivreId(133);
        for(Auteur a : lstAuteur) {
            System.out.println(a);
        }
        
        lstAuteur=auteurRepository.findBytop5Auteur();
        for(Auteur a : lstAuteur) {
            System.out.println(a);
        }
        
    }
}
