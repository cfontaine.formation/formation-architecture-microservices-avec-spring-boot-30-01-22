package fr.dawan.lambda;

 @FunctionalInterface
public interface ICompare<T> {
    
    boolean comp(T a,T b);

}
