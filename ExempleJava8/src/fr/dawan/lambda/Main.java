package fr.dawan.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
		
		// Expression Lambda
        int tab[]= {4,7,-8,1,0};
        
        Superieur sup=new Superieur();
        System.out.println(getValCmp(tab, sup));
        
        System.out.println(getValCmp(tab, new ICompare<Integer>() {
            
            @Override
            public boolean comp(Integer a, Integer b) {
                return a>b;
            }
        }));

        System.out.println("maximum=" + getValCmp(tab, ( v1,  v2) -> v1>v2));
        System.out.println("minimum=" + getValCmp(tab, ( v1,  v2) -> v1<v2));
    

        // Interfaces fonctionnelles
        List<Integer> lst=Arrays.asList(4,7,9,1,-3,2,-1,2,3,8);
        
        for (int i: lst) {
            System.out.println(i);
        }
		
		// Références de méthodes
        lst.forEach(m ->  System.out.println(m));
		
        lst.forEach(System.out::println);
        System.out.println("___________________________________");
        
		// Stream
        lst.stream().map(i -> i*2).filter(v -> v>5).sorted().limit(5).forEach(System.out::println);
        
		List <Integer> lst2=lst.stream().map(i -> i*2).filter(v -> v>5).sorted().limit(5).collect(Collectors.toList());
        
		int res=lst.stream().map(i -> i*2).filter(v -> v>5).sorted().limit(5).reduce(0, (a,b) -> a+b);
		System.out.println(res);
    }
    
    public static int getValCmp(int[] t, ICompare<Integer> cmp) {
        int m=t[0];
        for(int v : t) {
            if(cmp.comp(v, m))  {
                m=v;
            }
        }
        return m;
    }
    
    

}
