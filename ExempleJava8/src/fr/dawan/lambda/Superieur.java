package fr.dawan.lambda;

public class Superieur implements ICompare<Integer> {

    @Override
    public boolean comp(Integer a, Integer b) {
        return a>b;
    }

}
