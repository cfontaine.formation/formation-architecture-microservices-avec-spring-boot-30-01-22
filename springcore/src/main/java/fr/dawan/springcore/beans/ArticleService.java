package fr.dawan.springcore.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(value = "serviceA")
@Scope("prototype")
public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //@Autowired//(required = false)
   // @Qualifier("daoB")
    private ArticleDao dao;


    public ArticleService() {
        System.out.println("Constructeur par défaut :service");
    }

    @Autowired
    public ArticleService(@Qualifier("daoB")ArticleDao dao) {
        this.dao = dao;
        System.out.println("Constructeur un paramètre:service");
    }

    public ArticleDao getDao() {
        return dao;
    }

    //@Autowired
    //@Qualifier("daoB")
    public void setDao(/*@Qualifier("daoB")*/ ArticleDao dao) {
        System.out.println("setter ArticleService");
        this.dao = dao;
    }

    @Override
    public String toString() {
        return "ArticleService [dao=" + dao + ", toString()=" + super.toString() + "]";
    }
    
    @PostConstruct
    public void init() {
        System.out.println("Init");
    }
    
    @PreDestroy
    public void destroy() {
        System.out.println("Destroy");
    }
    
}
