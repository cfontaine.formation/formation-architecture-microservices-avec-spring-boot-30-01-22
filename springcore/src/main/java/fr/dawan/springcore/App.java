package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.ArticleDao;
import fr.dawan.springcore.beans.ArticleService;

public class App 
{
    public static void main( String[] args )
    {
       ApplicationContext ctx=new AnnotationConfigApplicationContext(AppConfig.class);
       System.out.println("-------------------------------------");
       
       ArticleDao daoA=ctx.getBean("daoA",ArticleDao.class);
       ArticleDao daoB=ctx.getBean("daoB",ArticleDao.class);
       System.out.println(daoA);
       System.out.println(daoB);
       
       ArticleService serviceA=ctx.getBean("serviceA",ArticleService.class);
       System.out.println(serviceA);
       serviceA=ctx.getBean("serviceA",ArticleService.class);
       System.out.println(serviceA);
       ((AbstractApplicationContext)ctx).close();
    }
}
