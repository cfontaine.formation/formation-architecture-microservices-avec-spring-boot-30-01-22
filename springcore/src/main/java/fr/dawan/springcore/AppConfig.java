package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.springcore.beans.ArticleDao;
import fr.dawan.springcore.beans.ArticleService;

@Configuration
@ComponentScan(basePackages = "fr.dawan.springcore")
public class AppConfig {
    
    @Bean
    public ArticleDao daoA() {
        return new ArticleDao("data1");
    }

    
    @Bean
    public ArticleDao daoB() {
        return new ArticleDao("data2");
    }
    
//    @Bean
//    public ArticleService serviceA() {
//        return new ArticleService();
//    }
    
//    @Bean(initMethod = "init",destroyMethod = "destroy")
//    public ArticleService serviceA(ArticleDao daoB) {
//        return new ArticleService(daoB);
//    }
}
