package fr.dawan.springboot;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.springboot.entities.Employe;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.repositories.EmployeCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.repositories.ProduitRepository;

@SpringBootTest
@ActiveProfiles("DEV")
public class RepositoryApplication {

    @Autowired
    private MarqueRepository marqueRepository;

    @Autowired
    private ProduitRepository produitRepository;
    
    @Autowired
    private EmployeCustomRepository employeRepository;

//    @Test
//    void persistMarque() {
//        Marque mA=new Marque("Marque A") ;
//        System.out.println(mA);
//        mA=marqueRepository.save(mA);
//        System.out.println(mA);
//    }

    @Test
    void findAllMarque() {
        List<Marque> lst = marqueRepository.findAll();
        for (Marque m : lst) {
            System.out.println(m);
        }
    }

    @Test
    void findByNom() {
        List<Marque> lst = marqueRepository.findByNom("Marque B");
        for (Marque m : lst) {
            System.out.println(m);
        }

        lst = marqueRepository.findByNom("Marque T");
        for (Marque m : lst) {
            System.out.println(m);
        }

        System.out.println(marqueRepository.existsByNom("Marque C"));
        System.out.println(marqueRepository.existsByNom("Marque U"));
    }

    @Test
    void findPrixProduit() {
        List<Produit> lst = produitRepository.findByPrixLessThanOrderByPrixDesc(100.0);
//       for(Produit p:lst) {
//           System.out.println(p);
//       }
        lst.forEach(System.out::println);

        lst = produitRepository.findByPrixBetween(40.0, 150.0);
        for (Produit p : lst) {
            System.out.println(p);
        }

        lst = produitRepository.findByDescriptionLikeIgnoreCase("%o");
        lst.forEach(System.out::println);
    }

    @Test
    void queryProduit() {
        List<Produit> lst = produitRepository.findByPrixInfJPQL(100.0);
        for (Produit p : lst) {
            System.out.println(p);
        }

        lst = produitRepository.findByPrixInfSQL(100.0);
        for (Produit p : lst) {
            System.out.println(p);
        }

    }

    @Test
    void pagination() {
        Page<Produit> p = produitRepository.findByPrixLessThan(200.0, PageRequest.of(1, 3));
        System.out.println(p.getTotalPages() + " " + p.getTotalElements());
        List<Produit> lst = p.getContent();
        for (Produit pr : lst) {
            System.out.println(pr);
        }

        p = produitRepository.findByPrixLessThan(200.0, PageRequest.of(1, 3, Sort.by("prix").descending()));
        System.out.println(p.getTotalPages() + " " + p.getTotalElements());
        lst = p.getContent();
        for (Produit pr : lst) {
            System.out.println(pr);
        }

        p = produitRepository.findByPrixLessThan(200.0, Pageable.unpaged());
        System.out.println(p.getTotalPages() + " " + p.getTotalElements());
        lst = p.getContent();
        for (Produit pr : lst) {
            System.out.println(pr);
        }

        lst = produitRepository.findByPrixLessThan(200.0, Sort.by("dateProduction").descending().by("prix"));
        for (Produit pr : lst) {
            System.out.println(pr);
        }

        lst = produitRepository.findTop3ByOrderByPrix();
        for (Produit pr : lst) {
            System.out.println(pr);
        }
    }

    @Test
    void procedureStockee() {
        int c = produitRepository.countByPrixSQL(40.0);
        System.out.println("SQL=" +c);
        c=produitRepository.countByPrix(40.0);
        System.out.println(c);
        c=produitRepository.get_count_by_prix2(40.0);
        System.out.println(c);
    }

    @Test
    void customRepository() {
        List<Employe> lst=employeRepository.findBy("John", null);
        for (Employe e : lst) {
            System.out.println(e);
        }
        
        lst=employeRepository.findBy(null, "Doe");
        for (Employe e : lst) {
            System.out.println(e);
        }
        
        lst=employeRepository.findBy("John", "Doe");
        for (Employe e : lst) {
            System.out.println(e);
        }
    }
}
