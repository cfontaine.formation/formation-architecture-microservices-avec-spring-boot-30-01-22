package fr.dawan.springboot;

import java.util.List;

import org.hibernate.internal.build.AllowSysOut;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.interfaces.MarqueService;

@SpringBootTest
@ActiveProfiles("DEV")
public class ServiceApplication {
    
    @Autowired
    private MarqueService marqueService;
    
    @Test
    public void marque() {
       List<MarqueDto> lst= marqueService.getAllMarque(Pageable.unpaged());
       for(MarqueDto md : lst) {
           System.out.println(md);
       }
       
       MarqueDto md2=marqueService.getMarqueById(2);
       System.out.println(md2);
       
       MarqueDto md3=new MarqueDto("MarqueD");
       System.out.println(md3);
       md3=marqueService.saveOrUpdate(md3);
       System.out.println(md3);
       
       marqueService.deleteById(md3.getId());
    }
}
