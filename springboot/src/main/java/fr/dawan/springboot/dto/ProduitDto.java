package fr.dawan.springboot.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import fr.dawan.springboot.enums.Conditionnement;

public class ProduitDto {
    
    private long id;
    
    private double prix;
    
    @NotBlank
    @Size(max=100)
    private String description; 
    
    private Conditionnement embalage;
    
    private LocalDate dateProduction;
    
    private MarqueDto marque;
    
    private byte[] image;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    public MarqueDto getMarque() {
        return marque;
    }

    public void setMarque(MarqueDto marque) {
        this.marque = marque;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Conditionnement getEmbalage() {
        return embalage;
    }

    public void setEmbalage(Conditionnement embalage) {
        this.embalage = embalage;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    } 
}
