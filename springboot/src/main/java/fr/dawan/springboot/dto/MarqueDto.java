package fr.dawan.springboot.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "marque")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MarqueDto {
    
    private long id;
    
    private String nom;

    public MarqueDto() {
    }

    public MarqueDto(String nom) {
        this.nom = nom;
    }

    @XmlAttribute
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @XmlElement
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "MarqueDto [id=" + id + ", nom=" + nom + "]";
    }
}
