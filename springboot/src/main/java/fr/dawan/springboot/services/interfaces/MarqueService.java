package fr.dawan.springboot.services.interfaces;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.MarqueDto;

public interface MarqueService {
    
    List<MarqueDto> getAllMarque(Pageable page);
    
    MarqueDto getMarqueById(long id);
    
    MarqueDto saveOrUpdate(MarqueDto marqueDto);
    
    void deleteById(long id);
}
