package fr.dawan.springboot.services;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.entities.Marque;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.services.interfaces.MarqueService;

@Service
public class MarqueServiceImplement implements MarqueService {

    @Autowired
    private MarqueRepository repository;
    
    @Autowired
    private ModelMapper mapper;
    
    @Override
    public List<MarqueDto> getAllMarque(Pageable page) {
//       List<Marque> lst = repository.findAll(page).getContent();
//       List<MarqueDto> lstDto=new ArrayList<>();
//       for(Marque m : lst) {
//           lstDto.add(mapper.map(m, MarqueDto.class));
//       }
//      return lstDto;
//        
        return repository.findAll(page).stream().map(m -> mapper.map(m, MarqueDto.class)).collect(Collectors.toList());
    }

    @Override
    public MarqueDto getMarqueById(long id) {
       return mapper.map(repository.findById(id).get(),MarqueDto.class);
    }

    @Override
    public MarqueDto saveOrUpdate(MarqueDto marqueDto) {
        Marque m=repository.saveAndFlush(mapper.map(marqueDto, Marque.class));
        return mapper.map(m, MarqueDto.class);
    }

    @Override
    public void deleteById(long id) {
        repository.deleteById(id);
    }

}
