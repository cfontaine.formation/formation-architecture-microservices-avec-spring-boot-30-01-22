package fr.dawan.springboot.services.interfaces;

import java.util.List;

import org.springframework.data.domain.Pageable;

import fr.dawan.springboot.dto.ProduitDto;

public interface ProduitService {
    
    List<ProduitDto> getAllProduit(Pageable page);
    
    ProduitDto getProduitById(long id);
    
    List<ProduitDto> getProduitByDescription(String Description);
    
    ProduitDto saveOrUpdate(ProduitDto produit);
    
    void deleteProduit(long id);
}
