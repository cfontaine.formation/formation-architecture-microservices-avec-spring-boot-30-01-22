package fr.dawan.springboot.services;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.repositories.ProduitRepository;
import fr.dawan.springboot.services.interfaces.ProduitService;
@Service
public class ProduitServiceImplement implements ProduitService {

    @Autowired
    private ProduitRepository prodRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Override
    public List<ProduitDto> getAllProduit(Pageable page) {
        List<Produit>  lst=prodRepository.findAll(page).getContent();
        List<ProduitDto> lstDto=new ArrayList<>();
        for(Produit p : lst) {
            lstDto.add(modelMapper.map(p, ProduitDto.class));
        }
        return lstDto;
    }

    @Override
    public ProduitDto getProduitById(long id) {
        return modelMapper.map(prodRepository.findById(id).get(), ProduitDto.class);
    }

    @Override
    public List<ProduitDto> getProduitByDescription(String Description) {
        List<Produit>  lst=prodRepository.findByDescriptionLikeIgnoreCase(Description);
        List<ProduitDto> lstDto=new ArrayList<>();
        for(Produit p : lst) {
            lstDto.add(modelMapper.map(p, ProduitDto.class));
        }
        return lstDto;
    }

    @Override
    public ProduitDto saveOrUpdate(ProduitDto produitDto) {
        Produit p= modelMapper.map(produitDto, Produit.class);
        p= prodRepository.saveAndFlush(p);
        return modelMapper.map(p, ProduitDto.class);
    }

    @Override
    public void deleteProduit(long id) {
        prodRepository.deleteById(id);

    }

}
