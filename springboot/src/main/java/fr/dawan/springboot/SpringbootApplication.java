package fr.dawan.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
	//	SpringApplication.run(SpringbootApplication.class, args);
	    SpringApplication app=new SpringApplication(SpringbootApplication.class);
	//    app.setAddCommandLineProperties(false);
	    app.run(args);
	}
	
	@Bean
	ModelMapper modelMapper(){
	    return new ModelMapper();
	}

}
