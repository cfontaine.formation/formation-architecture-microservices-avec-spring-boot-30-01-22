package fr.dawan.springboot.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.interfaces.MarqueService;

@Controller
@RequestMapping("/mvc")
public class MvcController {
    
    @Autowired 
    MarqueService service;
    
    @GetMapping("marque/{id}")
    public String exemple(Model model,@PathVariable int id) {
        MarqueDto  marque=service.getMarqueById(id);
        System.out.println(marque);
        model.addAttribute("marque", marque);
        return "exemple";
    }

}
