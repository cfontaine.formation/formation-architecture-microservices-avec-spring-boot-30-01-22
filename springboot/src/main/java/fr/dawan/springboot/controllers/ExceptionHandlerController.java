package fr.dawan.springboot.dto;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.dawan.springboot.utils.ApiError;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    
    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handlerException(Exception e,WebRequest request){
        ApiError apiErr=new ApiError(HttpStatus.I_AM_A_TEAPOT, e.getMessage());
        return handleExceptionInternal(e, apiErr, new HttpHeaders(), HttpStatus.I_AM_A_TEAPOT, request);
    }
}
