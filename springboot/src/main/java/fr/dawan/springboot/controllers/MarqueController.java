package fr.dawan.springboot.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.services.interfaces.MarqueService;

@RestController
@RequestMapping("api/marques")
public class MarqueController {
    
    @Autowired
    MarqueService service;
    
    @GetMapping(produces = {"application/json","application/xml"})
    public List<MarqueDto>getAllMarque() {
        return service.getAllMarque(Pageable.unpaged());
    }
    
    @GetMapping(params= {"page","size"},produces = "application/json")
    public List<MarqueDto>getAllMarque(Pageable page){
        return service.getAllMarque(page);
    }

    @GetMapping(value="/{id}", produces = "application/json") 
    public MarqueDto getById(@PathVariable long id) {
        return service.getMarqueById(id);
    }
    
    @DeleteMapping(value="/{id}",produces="text/plain" )
    public String remove(@PathVariable long id) {
        service.deleteById(id);
        return "La marque id=" + id + " est supprimé";
    }
    
    @PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto save(@RequestBody MarqueDto marqueDto)
    {
        return service.saveOrUpdate(marqueDto);
    }
    
    
    @PutMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MarqueDto update(@RequestBody MarqueDto marqueDto)
    {
        return service.saveOrUpdate(marqueDto);
    }
}
