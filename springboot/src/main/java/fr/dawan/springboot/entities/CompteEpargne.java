package fr.dawan.springboot.entities;

import javax.persistence.Entity;

@Entity
public class CompteEpargne extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    private double taux = 0.01;

    public CompteEpargne() {
        super();
    }

    public CompteEpargne(String titulaire, double solde, String iban, double taux) {
        super(titulaire, solde, iban);
        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }

}
