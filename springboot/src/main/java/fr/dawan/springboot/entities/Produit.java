package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import fr.dawan.springboot.enums.Conditionnement;

@Entity
@Table(name = "produits")
// @TableGenerator(name="produitGen")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @GeneratedValue(strategy =GenerationType.TABLE,generator = "produitGen")
    private long id;

    @Version
    private int version;

    @Column(length = 100, nullable = false)
    private String description;

    @Column(nullable = false)
    private double prix;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Conditionnement embalage;

    @Column(name = "date_production", nullable = false)
    private LocalDate dateProduction;
    
    @Lob
    private byte[] image;

//    @Transient
//    private int nePasPersister;

    @ManyToOne
    @JoinColumn(name = "id_marque", referencedColumnName = "id")
    private Marque marque;

    @ManyToMany
    @JoinTable(name = "produits2fournisseurs", joinColumns = @JoinColumn(name = "id_produit", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "id_fournisseur", referencedColumnName = "id"))
    private List<Fournisseur> fournisseurs = new ArrayList<>();

    public Produit() {

    }

    public Produit(String description, double prix, LocalDate dateProduction) {
        this(description, prix, Conditionnement.SANS, dateProduction);
    }

    public Produit(String description, double prix, Conditionnement embalage, LocalDate dateProduction) {
        this.description = description;
        this.prix = prix;
        this.embalage = embalage;
        this.dateProduction = dateProduction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Conditionnement getEmbalage() {
        return embalage;
    }

    public void setEmbalage(Conditionnement embalage) {
        this.embalage = embalage;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public List<Fournisseur> getFournisseurs() {
        return fournisseurs;
    }

    public void setFournisseurs(List<Fournisseur> fournisseurs) {
        this.fournisseurs = fournisseurs;
    }
    
    

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Produit [id=" + id + ", description=" + description + ", prix=" + prix + ", embalage=" + embalage
                + ", dateProduction=" + dateProduction + "]";
    }

}
