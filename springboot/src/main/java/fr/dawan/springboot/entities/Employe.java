package fr.dawan.springboot.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Version;

@Entity
@Table(name = "employes")
@TableGenerator(name="employeGen")
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   // @GeneratedValue(strategy =GenerationType.TABLE,generator = "employeGen")
    private long id;

    @Version
    private int version;
    
    @Column(length = 50)
    private String prenom;

    @Column(length = 50, nullable = false)
    private String nom;

    @Embedded
    private Adresse adressePersonelle;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "rue", column = @Column(name = "rue_pro")),
            @AttributeOverride(name = "ville", column = @Column(name = "ville_pro", length=50)),
            @AttributeOverride(name = "codePostal", column = @Column(name = "code_postal_pro", length=8)) })
    private Adresse adressePro;
    
    @ElementCollection
    @CollectionTable(name="emp_attrib")
    private List<String> attribution;

    public Employe() {
    }

    public Employe(String prenom, String nom, Adresse adressePersonelle) {
        this.prenom = prenom;
        this.nom = nom;
        this.adressePersonelle = adressePersonelle;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdressePersonelle() {
        return adressePersonelle;
    }

    public void setAdressePersonelle(Adresse adressePersonelle) {
        this.adressePersonelle = adressePersonelle;
    }

    @Override
    public String toString() {
        return "Employe [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", adressePersonelle=" + adressePersonelle
                + "]";
    }

}
