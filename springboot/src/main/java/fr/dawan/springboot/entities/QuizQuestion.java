package fr.dawan.springboot.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="questions")
public class QuizQuestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Version
    private int version;
    
    @Column(nullable = false,name= "texte")
    private String qstText;
    
    @ManyToOne
    private Quiz quiz;

    @Column(nullable = false)
    private boolean multiple;
    
    @Column(name="num_order")
    private int numOrder;

    public QuizQuestion() {
    }

    public QuizQuestion(String qstText, boolean multiple, int numOrder) {
        this.qstText = qstText;
        this.multiple = multiple;
        this.numOrder = numOrder;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQstText() {
        return qstText;
    }

    public void setQstText(String qstText) {
        this.qstText = qstText;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public boolean isMultiple() {
        return multiple;
    }

    public void setMultiple(boolean multiple) {
        this.multiple = multiple;
    }

    public int getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(int numOrder) {
        this.numOrder = numOrder;
    }

    @Override
    public String toString() {
        return "QuizQuestion [id=" + id + ", qstText=" + qstText + ", multiple=" + multiple + ", numOrder=" + numOrder
                + "]";
    }

}
