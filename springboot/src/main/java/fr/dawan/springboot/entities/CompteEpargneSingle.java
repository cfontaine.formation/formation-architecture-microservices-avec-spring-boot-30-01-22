package fr.dawan.springboot.entities;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("compte_epargne")
public class CompteEpargneSingle extends CompteBancaire {

    private static final long serialVersionUID = 1L;

    private double taux = 0.01;

    public CompteEpargneSingle() {
        super();
    }

    public CompteEpargneSingle(String titulaire, double solde, String iban, double taux) {
        super(titulaire, solde, iban);
        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    @Override
    public String toString() {
        return "CompteEpargne [taux=" + taux + ", toString()=" + super.toString() + "]";
    }

}
