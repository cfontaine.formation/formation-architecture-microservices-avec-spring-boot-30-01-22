package fr.dawan.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
@Entity
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.JOINED)
public class CompteBancaire extends AbstractEntity{

    private static final long serialVersionUID = 1L;
    
    @Column(length = 60, nullable = false)
    private String titulaire;
    
    @Column(nullable = false)
    private double solde;
    
    @Column(length = 40, nullable = false)
    private String iban;

    public CompteBancaire() {
    }

    public CompteBancaire(String titulaire, double solde, String iban) {
        this.titulaire = titulaire;
        this.solde = solde;
        this.iban = iban;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    @Override
    public String toString() {
        return "CompteBancaire [titulaire=" + titulaire + ", solde=" + solde + ", iban=" + iban + "]";
    }
    
}
