package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Marque;

@Repository
public interface MarqueRepository extends JpaRepository<Marque, Long> {
    
    List<Marque> findByNom(String nom);
    
    boolean existsByNom(String name);

}
