package fr.dawan.springboot.repositories;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {
    
    List<Produit> findByPrixLessThanOrderByPrixDesc(double prix);
    
    List<Produit> findByPrixBetween(double min, double max);
    
    List<Produit> findByDescriptionLikeIgnoreCase(String description);
    
    // JPQL
    @Query(value = "SELECT p FROM Produit p WHERE p.prix<:montant") // ?1
    List<Produit> findByPrixInfJPQL(@Param("montant")double prixMax);
    
    // SQL
    @Query(nativeQuery = true, value="SELECT * FROM produits WHERE prix< :montant")
    List<Produit> findByPrixInfSQL(@Param("montant")double prixMax);
    
    Page<Produit> findByPrixLessThan(double prix,Pageable pageable);
    
    List<Produit> findByPrixLessThan(double prix,Sort sort);
    
    List<Produit> findTop3ByOrderByPrix();
    
    @Query(value="CALL GET_COUNT_BY_PRIX(:montant)",nativeQuery = true)
    int countByPrixSQL(@Param("montant") double montant);
    
    // Appel explicite
    @Procedure("GET_COUNT_BY_PRIX2")
    int countByPrix(double montant);
    
    // Appel implicite
    @Procedure
    int get_count_by_prix2(@Param("montant") double montant);
}
