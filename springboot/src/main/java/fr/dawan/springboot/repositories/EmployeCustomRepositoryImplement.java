package fr.dawan.springboot.repositories;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Employe;

@Repository
public class EmployeCustomRepositoryImplement implements EmployeCustomRepository {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public List<Employe> findBy(String prenom, String nom) {
        String req="SELECT e FROM Employe e";
        Map<String,Object> params=new HashMap<>();
        if(prenom!=null) {
            req+=" WHERE e.prenom= :prenom";
            params.put("prenom", prenom);
        }
        if(nom!=null) {
            if(req.contains("WHERE")) {
                req+=" AND e.nom= :nom";
            }
            else {
                req+=" WHERE  e.nom= :nom";
            }
            params.put("nom", nom);
        }
        
       TypedQuery<Employe> tq = em.createQuery(req, Employe.class);
       for(Entry<String,Object> e : params.entrySet()) {
           tq.setParameter(e.getKey(), e.getValue());
       }
        return tq.getResultList();
    }

}
